<?php

namespace Altra\Dto;

use Altra\Dto\Contracts\DtoContract;
use Illuminate\Database\Eloquent\Model;

abstract class DataTransfer implements DtoContract
{
    public static function fromRequest(mixed $request): static
    {
        $properties = get_class_vars(static::class);
        $data = [];
        foreach ($properties as $property => $value) {
            $data[$property] = $request->get($property);
        }

        return new static(...$data);
    }

    public static function fromArray(array $array = []): static
    {
        $properties = get_class_vars(static::class);
        $data = [];
        foreach ($properties as $property => $value) {
            $data[$property] = $array[$property] ?? null;
        }

        return new static(...$data);
    }

    public static function fromArrayWithTranslations(array $array = []): static
    {
        $properties = get_class_vars(static::class);
        $data       = [];
        foreach ($properties as $property => $value) {
            if ($property == 'translations') {
                foreach ($array[$property] as $translation) {
                    $data['translations'][$translation['locale']] = [
                        'locale'     => $translation['locale'],
                        'name'       => $translation['name'],
                        'country_id' => $array['id'],
                        'id'         => $translation['id'],
                    ];
                }
            } else {
                $data[$property] = $array[$property] ?? null;
            }
        }

        return new static(...$data);
    }

    public static function fromModel(Model $model): static
    {
        $properties = get_class_vars(static::class);
        $data = [];
        foreach ($properties as $property => $value) {
            $data[$property] = $model?->$property ?? null;
        }

        return new static(...$data);
    }

    public function toArray(): array
    {
        $properties = get_object_vars($this);
        $array = [];
        foreach ($properties as $property => $type) {
            $array[$property] = $this->$property;
        }

        return $array;
    }
}
